#ifndef _platilla1_H
#define	_platilla1_H

#include <mia/no_renderizadas/qt/platilla1/uiplatilla1.h>
#include <string>
#include <iostream>
using namespace std;

class HelloForm : public QDialog {
    Q_OBJECT
public:
    HelloForm();
    virtual ~HelloForm();
public slots:
    void textChanged(const QString& text);
    //string retornTexto(QString text2);
public:
    Ui::HelloForm widget;
};
#endif
/*
La classe para hacer possible la platilla se hace de este modo....
#include "HelloForm.h"

HelloForm::HelloForm() {
    widget.setupUi(this);
    connect(widget.nameEdit, SIGNAL(textChanged(const QString&)), this, SLOT(textChanged(const QString&)));
}

HelloForm::~HelloForm() {
}

void HelloForm::textChanged(const QString& text) {//aqui es donde va lo de la aplicacion
    if (0 < text.trimmed().length()) {
        widget.helloEdit->setText("Hello " + text.trimmed() + "!");
    } else {
        widget.helloEdit->clear();
    }
}


/*
